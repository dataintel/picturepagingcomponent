import { NgModule, ModuleWithProviders } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { CommonModule } from '@angular/common';
import { BaseWidgetPicturePagingComponent } from './src/base-widget.component';
import { MaterialModule } from '@angular/material';
export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  declarations: [
    BaseWidgetPicturePagingComponent
  ],
  exports: [
	BaseWidgetPicturePagingComponent
  ]
})
export class PicturePagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PicturePagingModule,
      providers: []
    };
  }
}
